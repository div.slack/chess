<?php

class Pawn extends Figure
{
    public function __toString()
    {
        return $this->isBlack ? '♟' : '♙';
    }

    public function getPossibleMoves($coordinates_from, $figures)
    {
        [$current_x_position, $current_y_position] = $coordinates_from;

        $possible_moves = [];


        if ($this->getCountMoves() === 0) {
            $first_move_x = $current_x_position;
            $between_current_and_destination_y = $this->isBlack ? $current_y_position - 1 : $current_y_position + 1;
            $first_move_y = $this->isBlack ? $current_y_position - 2 : $current_y_position + 2;
            if (empty($figures[$first_move_x][$first_move_y]) and empty($figures[$first_move_x][$between_current_and_destination_y])) {
                $first_move = $first_move_x . $first_move_y;
                array_push($possible_moves, $first_move);
            }


        }

        $usual_move_x = $current_x_position;
        $usual_move_y = $this->isBlack ? $current_y_position - 1 : $current_y_position + 1;
        if (empty($figures[$usual_move_x][$usual_move_y])) {
            $usual_move = $usual_move_x . $usual_move_y;
            array_push($possible_moves, $usual_move);
        }

        $attack_move_x = [chr(ord($current_x_position) - 1), chr(ord($current_x_position) + 1)];
        $attack_move_y = $this->isBlack ? $current_y_position - 1 : $current_y_position + 1;
        foreach ($attack_move_x as $attack_move_x_item) {
            if (!empty($figures[$attack_move_x_item][$attack_move_y])) {
                $attack_move = $attack_move_x_item . $attack_move_y;
                array_push($possible_moves, $attack_move);
            }
        }

        return $possible_moves;
    }
}
