<?php

class Desk {
    private $figures = [];
    private $isCurrentMoveWhite = true;

    public function __construct() {
        $this->figures['a'][1] = new Rook(false);
        $this->figures['b'][1] = new Knight(false);
        $this->figures['c'][1] = new Bishop(false);
        $this->figures['d'][1] = new Queen(false);
        $this->figures['e'][1] = new King(false);
        $this->figures['f'][1] = new Bishop(false);
        $this->figures['g'][1] = new Knight(false);
        $this->figures['h'][1] = new Rook(false);

        $this->figures['a'][2] = new Pawn(false);
        $this->figures['b'][2] = new Pawn(false);
        $this->figures['c'][2] = new Pawn(false);
        $this->figures['d'][2] = new Pawn(false);
        $this->figures['e'][2] = new Pawn(false);
        $this->figures['f'][2] = new Pawn(false);
        $this->figures['g'][2] = new Pawn(false);
        $this->figures['h'][2] = new Pawn(false);

        $this->figures['a'][7] = new Pawn(true);
        $this->figures['b'][7] = new Pawn(true);
        $this->figures['c'][7] = new Pawn(true);
        $this->figures['d'][7] = new Pawn(true);
        $this->figures['e'][7] = new Pawn(true);
        $this->figures['f'][7] = new Pawn(true);
        $this->figures['g'][7] = new Pawn(true);
        $this->figures['h'][7] = new Pawn(true);

        $this->figures['a'][8] = new Rook(true);
        $this->figures['b'][8] = new Knight(true);
        $this->figures['c'][8] = new Bishop(true);
        $this->figures['d'][8] = new Queen(true);
        $this->figures['e'][8] = new King(true);
        $this->figures['f'][8] = new Bishop(true);
        $this->figures['g'][8] = new Knight(true);
        $this->figures['h'][8] = new Rook(true);
    }

    public function move($move) {
        if (!preg_match('/^([a-h])(\d)-([a-h])(\d)$/', $move, $match)) {
            throw new \Exception("Incorrect move");
        }

        $xFrom = $match[1];
        $yFrom = $match[2];
        $xTo   = $match[3];
        $yTo   = $match[4];



        if (isset($this->figures[$xFrom][$yFrom])) {
            $this->checkMoveDirection($this->figures[$xFrom][$yFrom]);
            $this->checkMoveCorrectness($this->figures[$xFrom][$yFrom],[$xFrom, $yFrom], [$xTo, $yTo]);

            $this->figures[$xTo][$yTo] = $this->figures[$xFrom][$yFrom];
            $this->isCurrentMoveWhite = !$this->isCurrentMoveWhite;
            $this->figures[$xTo][$yTo]->incrementCountMoves();

        }
        unset($this->figures[$xFrom][$yFrom]);
    }

    public function dump() {
        for ($y = 8; $y >= 1; $y--) {
            echo "$y ";
            for ($x = 'a'; $x <= 'h'; $x++) {
                if (isset($this->figures[$x][$y])) {
                    echo $this->figures[$x][$y];
                } else {
                    echo '-';
                }
            }
            echo "\n";
        }
        echo "  abcdefgh\n";
    }

    /**
     * @throws Exception
     */
    private function checkMoveDirection (Figure $figure) {
        $is_figure_white = $figure->getFigureColor() === 'White';

        if ($is_figure_white === $this->isCurrentMoveWhite) {
            return true;
        }else {
            throw new Exception('Another side should move.');
        }
    }

    /**
     * @param Figure $figure
     * @param array $coordinates_from
     * @param array $coordinates_to
     * @return bool
     * @throws Exception
     */
    private function checkMoveCorrectness (Figure $figure, Array $coordinates_from, Array $coordinates_to) {

        $string_coordinates_to = implode('', $coordinates_to);

        if (in_array($string_coordinates_to, $figure->getPossibleMoves($coordinates_from, $this->figures))) {
            return true;
        }else {

            throw new Exception(get_class($figure) . " can't move to {$string_coordinates_to}.");
        }


    }

}
