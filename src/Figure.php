<?php

class Figure {
    protected $isBlack;
    private $countMoves = 0;

    public function __construct($isBlack) {
        $this->isBlack = $isBlack;
    }

    /** @noinspection PhpToStringReturnInspection */
    public function __toString() {
        throw new \Exception("Not implemented");
    }

    public function getFigureColor () {
        return $this->isBlack ? 'Black' : 'White';
    }

    public function getCountMoves () {
        return $this->countMoves;
    }
    public function incrementCountMoves() {
        $this->countMoves = $this->countMoves + 1;
    }

    /**
     * В задумке это должен быть абстрактный метод, но тогда придётся
     * во всех фигурах его реализовывать.
     *
     * @return Array
     */
    public function getPossibleMoves ($coordinates_from, $figures) {
        $possible_moves = [];
        foreach (range(1, 8) as $row) {
            foreach (range('a', 'h') as $column) {
                $possible_moves[] = $column . $row;
            }
        }
        return $possible_moves;
    }

}
